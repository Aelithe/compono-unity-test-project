#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : Exports.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 23/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A utility class to allow Exporting files to the Export Output folder.
 */

using System.IO;

namespace Screenshot_Tool.Utilities
{
    public static class Exports
    {
        #region Variables.
        #region Public.
        // The naming convention to use for exported files.
        public static string namingConvention = "frame";
        // Index formatting.
        public static string indexFormat = "0000";
        #endregion
        #endregion


        #region Utility Methods.
        // Export the file given the folder name and index.
        public static bool ExportImageFile(string objectName, ref int frameIndex, ref byte[] bytes)
        {
            string path = Paths.ValidateExportFolder(objectName);
            
            // Check if we have a valid Export Output folder for this object.
            if (path != null)
            {
                FileStream f = File.Create(path + ExportImageName(ref frameIndex));
                // Produce the output file.
                if (f != null) {
                    // Write the details to file.
                    f.Write(bytes, 0, bytes.Length);

                    // We have successfully created this frame.
                    return true;
                }
            }
            // We were unable to create this frame.
            return false;
        }

        public static string ExportImageName(ref int frameIndex)
        {
            return namingConvention + frameIndex.ToString(indexFormat) + ".png";
        }
        #endregion
    }
}
#endif
