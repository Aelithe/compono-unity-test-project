#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : Imports.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 27/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A utility class to allow Importing pregabs into the tool for screenshotting.
 */

using System.Linq;
using UnityEngine;

namespace Screenshot_Tool.Utilities
{
    public enum ImportMethod
    {
        ManuallyCreatedList,
        AssetsResourcesFolder
    }

    public static class Imports
    {
        #region Variables.
        #region Public.

        #endregion
        
        #region Private.
        
        #endregion
        #endregion


        #region Utility Methods.
        // Return the list of prefabs found in the prefabs folder.
        public static GameObject[] ImportPrefabs(ImportMethod importMethod)
        {
            GameObject[] objects;
            string path = null;

            switch (importMethod)
            {
                case ImportMethod.ManuallyCreatedList:
                    return null;

                case ImportMethod.AssetsResourcesFolder:
                    path = Paths.ValidateResourcesFolder();

                    if (path == null)
                    {
                        Debug.Log($"Unable to access Import Input folder \"{path}\".");
                        return null;
                    }
                    
                    objects = Resources.LoadAll("Prefabs", typeof(GameObject)).Cast<GameObject>().ToArray();

                    if (objects == null)
                    {
                        Debug.LogError($"There are no prefabs to import from the \"{path}\" folder.");
                        return null;
                    }
                    break;

                default:
                    return null;
            }

            return objects;
        }
        #endregion
    }
}
#endif
