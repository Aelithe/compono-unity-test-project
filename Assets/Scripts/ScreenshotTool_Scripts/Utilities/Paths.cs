#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : Paths.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 23/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A utility class to allow access to the Export Output folder.
 */

using System.IO;
using UnityEngine;

namespace Screenshot_Tool.Utilities
{
    public static class Paths
    {
        #region Variables.
        #region Public.
        // The path to the Assets folder.
        public static string assetsPath = Application.dataPath + Path.DirectorySeparatorChar;
        // The path to the Resources folder.
        public static string resourcesPath = assetsPath + "Resources" + Path.DirectorySeparatorChar;
        // The path to the Project folder.
        public static string projectPath = Directory.GetParent(Application.dataPath).FullName + Path.DirectorySeparatorChar;
        // The path to the Import folder.
        public static string inputPath = projectPath + "Inputs" + Path.DirectorySeparatorChar;
        // The path to the Export Output folder.
        public static string outputPath = projectPath + "Outputs" + Path.DirectorySeparatorChar;
        #endregion
        #endregion


        #region Validate Methods.
        // Validation for the export folder ensuring we don't save content to a non-existent destination.
        public static string ValidateResourcesFolder(string folder = null)
        {
            string path = resourcesPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            // Check if the Assets Prefabs folder exists.
            if (GetFolder(path) == null)
            {
                // Create it so that we don't attempt to read files from a non-existent folder path.
                return CreateFolder(path);
            }
            // We have a valid Assets Prefabs folder.
            return path;
        }

        // Validation for the export folder ensuring we don't save content to a non-existent destination.
        public static string ValidateExportFolder(string folder = null)
        {
            string path = outputPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            // Check if the Export Output folder exists.
            if (GetFolder(path) == null)
            {
                // Create it so that we don't attempt to save files to a non-existent folder path.
                return CreateFolder(path);
            }
            // We have a valid Export Output folder.
            return path;
        }
        
        // Validation for the import folder ensuring we don't read content from a non-existent destination.
        public static string ValidateImportFolder(string folder = null)
        {
            string path = inputPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            // Check if the Export Input folder exists.
            if (GetFolder(path) == null)
            {
                // Create it so that we don't attempt to read files to a non-existent folder path.
                return CreateFolder(path);
            }
            // We have a valid Import Input folder.
            return path;
        }
        #endregion


        #region Get Methods.
        // Simply get the asset folder.
        public static string GetAssetsFolderPath(string folder = null)
        {
            string path = assetsPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            // Get and return the folder given our path.
            return GetFolder(path);
        }

        // Simply get the export folder.
        public static string GetResourcesFolderPath(string folder = null)
        {
            string path = resourcesPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            return GetFolder(path);
        }

        // Simply get the project folder.
        public static string GetProjectFolderPath(string folder = null)
        {
            string path = projectPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            // Get and return the folder given our path.
            return GetFolder(path);
        }

        // Simply get the export folder.
        public static string GetExportFolderPath(string folder = null)
        {
            string path = outputPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            return GetFolder(path);
        }

        // Simply get the import folder.
        public static string GetImportFolderPath(string folder = null)
        {
            string path = inputPath + (string.IsNullOrEmpty(folder) ? "" : folder + Path.DirectorySeparatorChar);
            return GetFolder(path);
        }
        #endregion


        #region Utility Methods.
        private static string GetFolder(string path)
        {
            // Check if the folder exists.
            if (Directory.Exists(path))
            {
                // We have a valid folder, we should now return the path.
                return path;
            }
            // We have no valid folder.
            return null;
        }

        private static string CreateFolder(string path)
        {
            if (Directory.CreateDirectory(path) == null)
            {
                // We don't have a valid folder.
                return null;
            }
            return path;
        }
        #endregion
    }
}
#endif
