#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : CameraController.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 28/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A controller class to allow access to the Main Camera Controller.
 */

using System.Linq;
using Screenshot_Tool.Utilities;
using UnityEngine;

namespace Screenshot_Tool.Controllers
{
    public class CameraController : MonoBehaviour
    {
        #region Variables.
        #region Public.
        public static CameraController singleton;

        [Header("Debugging Options")]
        [Tooltip("Enable Camera Controller specific Console Debug Logs (Errors are printed regardless of this option).")]
        public bool debugging = true;
        #endregion

        #region Private.
        [Header("Components")]
        [Tooltip("The camera to be controlled for screenshots.")]
        [SerializeField] private Camera screenshotCamera;

        [Header("Camera Options")]
        [Tooltip("The margin of space (percentage of object size) that should be between the object and the edge of the frame (Applied to all sides).")]
        [Range(0, 3)]
        [SerializeField] private float percentMargin = 0.1f;

        [Header("Image Options")]
        #pragma warning disable CS0649
        [Tooltip("Whether the Image should have a transparent background.")]
        [SerializeField] private bool transparentBackground;
        #pragma warning restore CS0649
        [Tooltip("The dimensions in pixels to be used when creating a screenshot.")]
        [SerializeField] private Vector2Int screenshotDimensions = new Vector2Int(512, 512);
        #endregion
        #endregion


        #region Unity Callbacks.
        void Awake()
        {
            if (singleton == null) singleton = this;
            if (screenshotCamera == null) screenshotCamera = Camera.main;
        }
        #endregion


        #region Utility Methods.
        // Setup the camera position and rotation based on the asset.
        public bool AlignTo(ref GameObject go)
        {
            // First check that we have a game object to use.
            if (go == null)
            {
                Debug.LogError("Unable to Setup Camera when there is no 'Game Object'!");
                return false;
            }

            // Otherwise we find the bounds of this object via the parent renderer first.
            Renderer parentRenderer = go.GetComponent<Renderer>();
            // Then we check the bounds of all child renderers.
            Renderer[] childrenRenderers = go.GetComponentsInChildren<Renderer>();
            // Find the first valid set of bounds to start from.
            Bounds bounds = parentRenderer != null
                ? parentRenderer.bounds
                : childrenRenderers.FirstOrDefault(x => x.enabled).bounds;
        
            // Calculate the bounds of all the renderers put together.
            if (childrenRenderers.Length > 0)
            {
                foreach (Renderer renderer in childrenRenderers)
                {
                    if (renderer.enabled)
                    {
                        bounds.Encapsulate(renderer.bounds);
                    }
                }
            }

            // Now find the distance the camera must be from the object in order to prevent clipping.
            float distance = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z) * 2;

            // Assign the size of the camera to the size of the largest side.
            screenshotCamera.orthographicSize = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z) / 2;
            // Now add the margin to the camera to pad the edges.
            screenshotCamera.orthographicSize += screenshotCamera.orthographicSize * percentMargin * 2;

            // Move camera in -z-direction; change '2.0f' to your needs
            screenshotCamera.transform.position = new Vector3(
                bounds.center.x,
                bounds.center.y,
                -distance);

            // Prompt the user that the camera has been setup correctly for the current object.
            if (debugging && ScreenshotToolController.singleton.debugging)
            {
                Debug.Log($"Successfully Setup the Camera for processing \"{go.name}\"!");
            }
            return true;
        }
        
        public void TakeScreenshot(ref GameObject go, ref int frameIndex)
        {
            // Take a reference to the previous flags so that we can reset it after using the camera.
            CameraClearFlags prevClearFlags = screenshotCamera.clearFlags;
            // Change the clear flags to allow transparent backgrounds.
            if (transparentBackground) screenshotCamera.clearFlags = CameraClearFlags.Depth;

            // Create a render texture to render onto the camera.
            RenderTexture rt = new RenderTexture(screenshotDimensions.x, screenshotDimensions.y, 24);
            screenshotCamera.targetTexture = rt;

            // Create a Teture to draw our screenshot to.
            Texture2D screenShot = new Texture2D(
                screenshotDimensions.x, screenshotDimensions.y,
                transparentBackground ? TextureFormat.ARGB32 : TextureFormat.RGB24,
                false);
            screenshotCamera.Render();
            RenderTexture.active = rt;

            // Extract the pixels to our screenshot texture.
            screenShot.ReadPixels(new Rect(0, 0, screenshotDimensions.x, screenshotDimensions.y), 0, 0);

            // Clear the render texture from our camera.
            screenshotCamera.targetTexture = null;
            RenderTexture.active = null;
            Destroy(rt);

            // Encode the bytes to a PNG Image format.
            byte[] bytes = screenShot.EncodeToPNG();

            // Export the file to the output folder.
            Exports.ExportImageFile(go.name, ref frameIndex, ref bytes);

            // Set the camera clear flags to their previous setting.
            screenshotCamera.clearFlags = prevClearFlags;

            // Prompt the user about the successful export of this file.
            if (debugging && ScreenshotToolController.singleton.debugging)
            {
                Debug.Log($"Exported PNG Image Frame of \"{go.name}\" for frame {Exports.ExportImageName(ref frameIndex)}!");
            }

        }
        #endregion
    }
}
#endif
