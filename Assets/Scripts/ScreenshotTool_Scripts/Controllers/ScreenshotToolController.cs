﻿#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : ScreenshotToolController.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 23/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: The main controller for the screenshot tool.
 */

using System.Threading.Tasks;
using Screenshot_Tool.Models;
using Screenshot_Tool.Utilities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Screenshot_Tool.Controllers
{
    public class ScreenshotToolController : MonoBehaviour
    {
        #region Veriables.
        #region Public.
        public static ScreenshotToolController singleton;

        [Header("Debugging Options")]
        [Tooltip("Enable Console Debug Logs (Errors are printed regardless of this option).")]
        public bool debugging = true;

        [Tooltip("The amount of time the tool should wait between the processing of objects.")]
        [Space(4)]
        [Min(0)]
        public float processDelay = 10;
        #endregion

        #region Private.
        #pragma warning disable CS0414
        [Header("Newly Spawned Object Setup")]
        [Tooltip("The position to place each newly spawned object.")]
        [SerializeField] private Vector3 spawnPosition = new Vector3();
        [Tooltip("The rotation for each newly spawned object.")]
        [SerializeField] private Quaternion spawnRotation = Quaternion.identity;
        [Tooltip("The parent object in the hierarchy for each newly spawned object.")]
        [SerializeField] private Transform parentTransform = null;
        #pragma warning restore CS0414

        [Header("Import Options")]
        [Tooltip("The process method to be used for processing assets.")]
        [SerializeField] private ImportMethod importMethod = ImportMethod.ManuallyCreatedList;

        [Header("Manually Created Items List")]
        [Tooltip("A list of assets that you can manually setup to be processed.")]
        [SerializeField] private SpawnItemList m_itemList = null;

        private AssetReferenceGameObject m_assetLoadedAsset;

        // A list of the assets that are automatically setup to be processed either via the Asset Prefabs folder or the Project Input folder.
        private GameObject[] m_objectList = null;

        private GameObject m_instanceObject = null;
        #endregion
        #endregion


        #region Unity Callbacks.
        void Awake()
        {
            if (!singleton) singleton = this;
        }

        async void Start()
        {
            if (!parentTransform) parentTransform = transform;

            #if !UNITY_EDITOR
            // The debugging option should always be false in a build to maximise processing speed.
            debugging = false;
            // The frame delay option should always be 0 in a build to maximise processing speed.
            frameDelay = 0;
            #endif

            // Validate Export folder.
            if (Paths.ValidateExportFolder() == null)
            {
                Debug.LogError($"Failed to locate/create project 'Export' folder!");
            }

            if (debugging) Debug.Log("Processing assets!");

            // Start processing the assets.         
            switch (importMethod)
            {
                case ImportMethod.ManuallyCreatedList:
                    if (debugging) Debug.Log("Manual List Asset Processing Started!");
                    await ProcessManualListAssetsAsync();
                    break;

                case ImportMethod.AssetsResourcesFolder:
                    if (debugging) Debug.Log("Prefab Asset Processing Started!");
                    await ProcessImportAssetsAsync();
                    break;

                default:
                    break;
            }

            // Finish Up.
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            #else
            Application.Quit();
            #endif
        }
        #endregion


        #region Processing Methods.
        // Process assets that have been brought into the tool via manual list creation.
        private async Task ProcessManualListAssetsAsync()
        {
            // Check if we have a list of objects to use.
            if (m_itemList == null || m_itemList.AssetReferenceCount == 0)
            {
                Debug.LogError("Manual item list not setup correctly!");
                Debug.LogError("Processing Failed!");
                return;
            }

            for (int i = 0; i < m_itemList.AssetReferenceCount; i++)
            {
                // Load the item.
                await LoadItemAtIndexAsync(i);
                
                // Prompt the user that processing has begun for the current object.
                if (debugging) Debug.Log($"Started Processing \"{m_instanceObject.name}\"!");

                // Setup the camera based on the object.
                CameraController.singleton.AlignTo(ref m_instanceObject);
                // Screenshot the asset.
                await GameObjectController.singleton.AlignAssetAsync(m_instanceObject);

                // Prompt the user about the successful processing of this object.
                if (debugging) Debug.Log($"Successfully Processed \"{m_instanceObject.name}\"!");
                // Wait for the delay of this process before continuing.
                await Task.Delay((int)(processDelay * 1000));
            }
            // Prompt the user about the successful processing of all the objects.
            if (debugging) Debug.Log("Processing Completed!");
        }

        // Process assets that have been brought into the tool via automatic import.
        private async Task ProcessImportAssetsAsync()
        {
            m_objectList = Imports.ImportPrefabs(importMethod);

            // Check if we have a list of objects to use.
            if (m_objectList == null)
            {
                Debug.LogError("Import paths/folders are not setup correctly!");
                Debug.LogError("Processing Failed!");
                return;
            }
            // Check if the list of objects has any objects to use.
            else if (m_objectList.Length < 1)
            {
                Debug.LogError("Import item list not setup correctly!");
                Debug.LogError("Processing Failed!");
                return;
            }

            for (int i = 0; i < m_objectList.Length; i++)
            {
                // Clear the previous asset.
                if (m_instanceObject != null)
                    DestroyImmediate(m_instanceObject);

                // Instantiate the asset.
                m_instanceObject = Instantiate(m_objectList[i], spawnPosition, spawnRotation, parentTransform);
                m_instanceObject.name = m_objectList[i].name;

                // Prompt the user that processing has begun for the current object.
                if (debugging) Debug.Log($"Started Processing \"{m_instanceObject.name}\"!");

                // Setup the camera based on the object.
                CameraController.singleton.AlignTo(ref m_instanceObject);
                // Screenshot the asset.
                await GameObjectController.singleton.AlignAssetAsync(m_instanceObject);

                // Prompt the user about the successful processing of this object.
                if (debugging) Debug.Log($"Successfully Processed \"{m_instanceObject.name}\"!");
                // Wait for the delay of this process before continuing.
                await Task.Delay((int)(processDelay * 100));
            }
            // Prompt the user about the successful processing of all the objects.
            if (debugging) Debug.Log("Processing Completed!");
        }
        #endregion


        #region Utility Methods.
        private async Task<GameObject> LoadItemAtIndexAsync(int index) 
        {
            // Clear the previous asset.
            if (m_instanceObject != null)
                Destroy(m_instanceObject);

            m_assetLoadedAsset = m_itemList.GetAssetReferenceAtIndex(index);

            TaskCompletionSource<GameObject> taskCompletionSource = new TaskCompletionSource<GameObject>();

            var loadRoutine = m_assetLoadedAsset.LoadAssetAsync();
            loadRoutine.Completed += (AsyncOperationHandle<GameObject> obj) => 
            {
                m_instanceObject = Instantiate(obj.Result, spawnPosition, spawnRotation, parentTransform);
                m_instanceObject.name = obj.Result.name;
                
                if (debugging) Debug.Log($"Instantiated \"{m_instanceObject.name}\" at Pos: {spawnPosition}, Rot: {spawnRotation.eulerAngles}, Parent: ({parentTransform.name}).");
                
                if (obj.OperationException != null)
                {
                    taskCompletionSource.SetException(obj.OperationException);
                }
                else
                {
                    taskCompletionSource.SetResult(obj.Result);
                }
            };

            return await taskCompletionSource.Task;
        }
        #endregion
    }
}
#endif
