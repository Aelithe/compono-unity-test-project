#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : GameObjectController.cs
 *
 * Created By: Tristyn Mackay.
 * Created Date: 28/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A controller class to allow access to the GameObject Controller.
 */

using System.Threading.Tasks;
using Screenshot_Tool.Utilities;
using UnityEngine;

namespace Screenshot_Tool.Controllers
{
    public class GameObjectController : MonoBehaviour
    {
        #region Variables.
        #region Public.
        public static GameObjectController singleton;

        [Header("Debugging Options")]
        [Tooltip("Enable Screenshot Controller specific Console Debug Logs (Errors are printed regardless of this option).")]
        public bool debugging = true;

        [Tooltip("The amount of time the tool should wait between the processing of frames.")]
        [Space(4)]
        [Min(0)]
        public float frameDelay = 10;
        #endregion

        #region Private.
        [Header("Screenshot Options")]
        [Tooltip("The amount of rotation that should be applied between frames.")]
        [Range(0.1f, 360)]
        // The amount of rotation that should be applied to an object between frames.
        [SerializeField] private float rotationValue = 22.5f;
        // The amount of frames as a result of a full 360 rotation divided by the rotation value.
        private int rotationAmount = 16;
        #endregion
        #endregion


        #region Unity Callbacks.
        void Awake()
        {
            if (singleton == null) singleton = this;
        }
        #endregion


        #region Utility Methods.
        public async Task AlignAssetAsync(GameObject go)
        {
            // Check if we have a game object to use.
            if (!go)
            {
                Debug.LogError("Unable to Take Screenshot when there is no 'Game Object'!");
                return;
            }

            // Calculate the current screenshot rotation amount to determine the total frames required.
            rotationAmount = (int)(360 / rotationValue);

            // Iterate over all frames setting them up and creating them.
            for (int r = 0; r < rotationAmount; r++)
            {
                // Rotate the object.
                go.transform.rotation = Quaternion.Euler(0, r * rotationValue, 0);
                // Take an image of the object.
                CameraController.singleton.TakeScreenshot(ref go, ref r);

                // Prompt the user about the successful processing of this object.
                if (debugging && ScreenshotToolController.singleton.debugging)
                {
                    Debug.Log($"Successfully took a Screenshot of \"{go.name}\" for the frame \"{Exports.ExportImageName(ref r)}\"!");
                }
                // Wait for the delay of this frame before continuing.
                await Task.Delay(GetTotalFrameDelay_Milliseconds());
            }
        }

        private float GetTotalFrameDelay_Seconds()
        {
            return rotationAmount * frameDelay;
        }
        private int GetTotalFrameDelay_Milliseconds()
        {
            return (int)(rotationAmount * frameDelay * 100);
        }
        #endregion
    }
}
#endif
