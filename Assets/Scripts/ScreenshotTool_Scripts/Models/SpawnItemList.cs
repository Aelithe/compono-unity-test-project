﻿#if UNITY_EDITOR || UNITY_STANDALONE
/* 
 * Filename : SpawnItemList.cs
 *
 * Created By: Compono.
 * Created Date: 22/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 23/03/2021.
 *
 * Description: A data structure class to allow Storing Lists of assets.
 */

using UnityEngine;
using UnityEngine.AddressableAssets;


namespace Screenshot_Tool.Models
{
    [CreateAssetMenu(fileName ="SpawnItemList.asset", menuName = "Screenshot Tool/SpawnItemList")]
    public class SpawnItemList : ScriptableObject
    {
        #region Veriables.
        #region Public.
        public int AssetReferenceCount => m_assetReferenceList.Length;
        #endregion

        #region Private.
        [SerializeField] private AssetReferenceGameObject[] m_assetReferenceList = new AssetReferenceGameObject[0];
        #endregion
        #endregion


        #region Utility Methods.
        public AssetReferenceGameObject GetAssetReferenceAtIndex(int index) 
        {
            return m_assetReferenceList[index];
        }
        #endregion
    }
}
#endif
