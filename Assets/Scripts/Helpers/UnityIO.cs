﻿#if UNITY_EDITOR
/* 
 * Filename : UnityIO.cs
 *
 * Created By: Compono.
 * Created Date: 22/03/2021.
 *
 * Edited By: Tristyn Mackay.
 * Edited Date: 28/03/2021.
 *
 * Description: A Helpoer class to allow menu options.
 */

using Screenshot_Tool.Utilities;
using UnityEditor;
using System.IO;

public static class UnityIO
{
    [MenuItem("Screenshot Tool/Open Project Folder", priority = 0)]
    public static void OpenProjectFolder() 
    {
        EditorUtility.RevealInFinder(Paths.ValidateExportFolder());
    }

    [MenuItem("Screenshot Tool/Delete Outputs Folder", priority = 20)]
    public static void DeleteOutputsFolder() 
    {
        Directory.Delete(Paths.outputPath, true);
    }
}
#endif
